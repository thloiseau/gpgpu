#include <CL/sycl.hpp>
#include <vector>
#include <fstream>
#include <iostream>
#include <chrono>
#include "bmp.hpp"

const size_t width = 1024;
const size_t height = 1024;
const size_t local_width = 1;
const size_t local_height = 1;
const size_t size = width * height;
const size_t imageSize = size * 3;
const size_t bufferSize = size * sizeof(float);

int main() {
  using namespace cl::sycl;
  std::vector<float> image(imageSize);

  auto startSYCL = std::chrono::system_clock::now();

  {
    // Creating buffer
    buffer<float, 1> myBuffer(image.data(), range<1>(image.size()));

    // Creating SYCL queue
    queue myQueue;

    // Submitting command group(work) to queue
    myQueue.submit([&](handler &cgh)
    {

      // Getting write only access to the buffer on a device
      auto Accessor = myBuffer.get_access<access::mode::write>(cgh);

      // Executing kernel in parallel
      cgh.parallel_for<class Mandelbrot>(range<2>(width, height), [=] (id<2> index){
        cl_int i = index.get(0);
        cl_int j = index.get(1);
        
        cl_float cr = ((cl_float)i / width) - 1.5;
        cl_float ci = ((cl_float)j / height) - 0.5;
        cl_float x = 0.0f;
        cl_float y = 0.0f;

        cl_int cmp = 0;
        while( x*x + y*y <= 4 && cmp <= 40){
          cl_float xtemp = x*x - y*y + cr;
          y = 2*x*y + ci;
          x = xtemp;
          ++cmp;
        }

        cl_float red = (cl_float)cmp/40;

        Accessor[(j * width + i) * 3] = red;
        Accessor[(j * width + i) * 3 + 1] = 0.0f;
        Accessor[(j * width + i) * 3 + 2] = 0.0f;
      });

      // Executing kernel with multiple invocations in a work-group
      /*cgh.parallel_for_work_group<class Mandelbrot>(
        range<2>(width / local_width, height / local_height),
        range<2>(local_width, local_height),
        [=](group<2> myGroup)
      {
        myGroup.parallel_for_work_item([&](h_item<2> item){
          cl_int i = item.get_global_id(0);
          cl_int j = item.get_global_id(1);
          
          cl_float cr = ((cl_float)i / width) - 1.5;
          cl_float ci = ((cl_float)j / height) - 0.5;
          cl_float x = 0.0f;
          cl_float y = 0.0f;

          cl_int cmp = 0;
          while( x*x + y*y <= 4 && cmp <= 40){
            cl_float xtemp = x*x - y*y + cr;
            y = 2*x*y + ci;
            x = xtemp;
            ++cmp;
          }

          cl_float red = (cl_float)cmp/40;

          Accessor[(j * width + i) * 3] = red;
          Accessor[(j * width + i) * 3 + 1] = 0.0f;
          Accessor[(j * width + i) * 3 + 2] = 0.0f;
        });
      })*/
    });
  }

  auto endSYCL = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds_SYCL = endSYCL - startSYCL;

  writeBufferAsBMP(image.data(), width, height, "Mandelbrot_SYCL.bmp");

  //----------------- Results -----------------

  printf("Compuation duration: %f\n", elapsed_seconds_SYCL.count());

  return 0;
}
