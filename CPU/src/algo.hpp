#ifndef ALGO_HPP
#define ALGO_HPP

#include <iostream>
#include <tuple>
#include "complexe.hpp"

class Algo {
	private:
		std::string name;
	protected:
		int width;
		int height;
		int iterationMax;
		Complexe z;
		Complexe c;
	public:
		virtual std::tuple<Complexe, Complexe> operator()(unsigned int i, unsigned int j) = 0;
		inline Algo(std::string n, int w, int h, int i) : name(n), width(w), height(h), iterationMax(i) {}
		inline const Complexe& getZ() const {
			return z;
		}
		inline const Complexe& getC() const {
			return c;
		}
		inline std::string getName() const {
			return name;
		}
		inline int getWidth() const {
			return width;
		}
		inline int getHeight() const {
			return height;
		}
		inline int getIterationMax() const {
			return iterationMax;
		}
};

class Julia : public Algo {
	public:
		inline Julia(int width, int height) : Algo("julia", width, height, 400) {
			c = Complexe(0.292, 0.015);
			z = Complexe();
		}
		inline std::tuple<Complexe, Complexe> operator()(unsigned int i, unsigned int j) {
			return std::tuple<Complexe, Complexe>(c, Complexe(static_cast<float>(i) / static_cast<float>(width), static_cast<float>(j) / static_cast<float>(height)));
		}
};

class Mandelbrot : public Algo {
	public:
		inline Mandelbrot(int width, int height) : Algo("mandelbrot", width, height, 40) {
			z = Complexe();
			c = Complexe();
		}
		inline std::tuple<Complexe, Complexe> operator()(unsigned int i, unsigned int j) {
			return std::tuple<Complexe, Complexe>(Complexe(static_cast<float>(i) / static_cast<float>(width) - 1.5, static_cast<float>(j) / static_cast<float>(height) - 0.5), z);
		}
};



#endif
