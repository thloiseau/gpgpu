#ifndef COMPLEXE_HPP
#define COMPLEXE_HPP

#include <cmath>

class Complexe{
    private:
        float r;
        float i;

    public:
        Complexe(float re = 0.0f, float im = 0.0f);
        float modulus() const;
        friend Complexe operator+(const Complexe &, const Complexe &);
        friend Complexe operator*(const Complexe &, const Complexe &);
};

#endif