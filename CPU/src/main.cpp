#include <fstream>
#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <tuple>
#include <typeinfo>
#include "bmp.hpp"
#include "complexe.hpp"
#include "algo.hpp"

void sequence(Algo * a, std::vector<float> & buffer){
	for(unsigned int i=0; i < a->getWidth(); ++i){
		for(unsigned int j=0; j < a->getHeight(); ++j){
			Complexe c, z;
			std::tuple<Complexe, Complexe> t = (*a)(i,j);
			c = std::get<0>(t);
			z = std::get<1>(t);


			unsigned int cmp = 0u;
			while(z.modulus() < 2 && cmp <= a->getIterationMax()){
				z = z * z + c;
				++cmp;
			}

			const float red = static_cast<float>(cmp)/static_cast<float>(a->getIterationMax());

			buffer[(j * a->getWidth() + i) * 3] = red;
			buffer[(j * a->getWidth() + i) * 3 + 1] = 0.0f;
			buffer[(j * a->getWidth() + i) * 3 + 2] = 0.0f;
		}
	}
}

void dispatch(Algo * a, std::vector<float> * buffer, unsigned int startX, unsigned int endX, unsigned int startY, unsigned int endY){
	for(unsigned int i=startX; i < endX; ++i){
		for(unsigned int j=startY; j < endY; ++j){
			Complexe c, z;
			std::tuple<Complexe, Complexe> t = (*a)(i,j);
			c = std::get<0>(t);
			z = std::get<1>(t);

			unsigned int cmp = 0u;
			while(z.modulus() < 2 && cmp <= a->getIterationMax()){
				z = z * z + c;
				++cmp;
			}

			const float green = static_cast<float>(cmp)/static_cast<float>(a->getIterationMax());

			(*buffer)[(j * a->getWidth() + i) * 3] = 0.0f;
			(*buffer)[(j * a->getWidth() + i) * 3 + 1] = green;
			(*buffer)[(j * a->getWidth() + i) * 3 + 2] = 0.0f;
		}
	}
}

int main(int argc, char *argv[]) {

	//----------------- Init -----------------
	int width = 1024;
	int height = 1024;
	Algo * a = new Mandelbrot(width, height);
	std::vector<float> buffer(a->getWidth() * a->getHeight() * 3);

	//----------------- Algo Sequential -----------------
	auto startSeq = std::chrono::system_clock::now();

	sequence(a, buffer);

	auto endSeq = std::chrono::system_clock::now();
	std::chrono::duration<double> seq_elapsed_seconds = endSeq -startSeq;

	writeBufferAsBMP(buffer.data(), a->getWidth(), a->getHeight(), a->getName() + ".bmp");

	//----------------- Algo Threads -----------------
	const unsigned int nbThreads = 16;
	std::thread threads[nbThreads];

	unsigned int div = static_cast<int>(sqrt(nbThreads));
	unsigned int hdiv = a->getWidth() / div;
	unsigned int wdiv = a->getHeight() / div;

	auto start = std::chrono::system_clock::now();
	
	for(int i = 0; i < div; ++i){
		for(int j=0; j < div; ++j){
			threads[i * div + j] = std::thread(dispatch, a, &buffer, i*hdiv, (i+1)*hdiv, j*wdiv, (j+1)*wdiv);
		}
	}
	for(int i = 0; i < nbThreads; ++i){
		threads[i].join();
	}

	auto end = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end -start;

	writeBufferAsBMP(buffer.data(), a->getWidth(), a->getHeight(), a->getName() + "_thread.bmp");

	//----------------- Results -----------------

	printf("Seq duration: %f\n", seq_elapsed_seconds.count());
	printf("Thread duration: %f\n", elapsed_seconds.count());

	delete a;

	return 0;
}
