cmake_minimum_required(VERSION 3.15.3 FATAL_ERROR)

find_package(OpenCL)

if(OpenCL_FOUND)
	add_executable(OpenCL OpenCL/src/main.cpp OpenCL/src/bmp.cpp OpenCL/src/bmp.hpp OpenCL/src/complexe.cpp OpenCL/src/complexe.hpp OpenCL/src/device_picker.h OpenCL/src/err_code.h OpenCL/device/mandelbrot.cl OpenCL/device/julia.cl)
	target_include_directories(OpenCL PRIVATE OpenCL::OpenCL)
	target_link_libraries(OpenCL OpenCL::OpenCL)
	set_target_properties(OpenCL PROPERTIES CXX_STANDARD 11)
endif(OpenCL_FOUND)