#include "complexe.hpp"

Complexe::Complexe(float re, float im) : r(re), i(im){}

float Complexe::modulus() const{
    return sqrt(r * r + i * i);
}

Complexe operator+(const Complexe & a, const Complexe & b){
    return Complexe(a.r + b.r, a.i + b.i);
}

Complexe operator*(const Complexe & a, const Complexe & b){
    return Complexe(a.r * b.r - a.i * b.i, a.r*b.i + a.i * b.r);
}