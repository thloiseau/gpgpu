#include <fstream>
#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <tuple>
#include <typeinfo>
#include <CL/cl.h>
#include "err_code.h"
#include "complexe.hpp"
#include "bmp.hpp"
#include "device_picker.h"

unsigned int width = 1024;
unsigned int height = 1024;
size_t local_width = 32;
size_t local_height = 1;
size_t size = (size_t)(width * height * 3);
size_t bufferSize = size * sizeof(float);
char kernelName[50] = "..\\OpenCL\\device\\mandelbrot.cl";
unsigned int iteration = 40;

int output_device_info(cl_device_id);

int main(int argc, char *argv[]) {

	//--------------------------------------------------------------------------------
	// Init
	//--------------------------------------------------------------------------------

	char* kernelsource;					// kernel source string

	cl_int				err;			// error code returned from OpenCL calls
	cl_device_id		device;			// compute device id
	cl_context			context;		// compute context
	cl_program			program;		// compute program
	cl_command_queue	commands;		// compute command queue
	cl_kernel			kernel;			// compute kernel
	cl_mem				b;				// GPU buffer

	std::vector<float> buffer(size);	// Host buffer

	auto startCL = std::chrono::system_clock::now();
	auto startInit = std::chrono::system_clock::now();

	//--------------------------------------------------------------------------------
	// Create a context, queue and device.
	//--------------------------------------------------------------------------------

	cl_uint deviceIndex = 0;
	parseArguments(argc, argv, &deviceIndex);

	// Get list of devices
	cl_device_id devices[MAX_DEVICES];
	unsigned numDevices = getDeviceList(devices);

	printf("#################### Devices informations ####################\n");
	for (unsigned int i = 0; i < numDevices; ++i) {
		printf("-------- Device %d --------\n", i);
		output_device_info(devices[i]);
		printf("\n");
	}

	// Check device index in range
	if (deviceIndex >= numDevices)
	{
		printf("Invalid device index (try '--list')\n");
		return EXIT_FAILURE;
	}

	device = devices[deviceIndex];	

	char name[MAX_INFO_STRING];
	getDeviceName(device, name);
	printf("\nUsing OpenCL device: %s\n", name);

	// Create a compute context
	context = clCreateContext(0, 1, &device, NULL, NULL, &err);
	checkError(err, "Creating context");
	// Create a command queue
	commands = clCreateCommandQueueWithProperties(context, device, 0, &err);
	checkError(err, "Creating command queue");


	//--------------------------------------------------------------------------------
	// Setup the buffer
	//--------------------------------------------------------------------------------

	b = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(float) * buffer.size(), NULL, &err);
	checkError(err, "Creating buffer");


	//--------------------------------------------------------------------------------
	// Get OpenCL kernel source file
	//--------------------------------------------------------------------------------

	FILE* file;
	fopen_s(&file, kernelName, "r");
	if (!file)
	{
		fprintf(stderr, "Error: Could not open kernel source file\n");
		exit(EXIT_FAILURE);
	}
	fseek(file, 0, SEEK_END);
	int len = ftell(file) + 1;
	rewind(file);

	kernelsource = (char*)calloc(sizeof(char), len);
	if (!kernelsource)
	{
		fprintf(stderr, "Error: Could not allocate memory for source string\n");
		exit(EXIT_FAILURE);
	}
	fread(kernelsource, sizeof(char), len, file);
	fclose(file);

	//--------------------------------------------------------------------------------
	// Create and Build Program from source file
	//--------------------------------------------------------------------------------

	// Create the compute program from the source buffer
	program = clCreateProgramWithSource(context, 1, (const char**)&kernelsource, NULL, &err);
	checkError(err, "Creating program with C_elem.cl");
	free(kernelsource);

	// Build the program
	err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
	if (err != CL_SUCCESS)
	{
		size_t len;
		char buffer[2048];

		printf("Error: Failed to build program executable!\n%s\n", err_code(err));
		clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
		printf("%s\n", buffer);
		return EXIT_FAILURE;
	}

	//--------------------------------------------------------------------------------
	// Create compute kernel
	//--------------------------------------------------------------------------------

	// Create the compute kernel from the program
	kernel = clCreateKernel(program, "mandelbrot", &err);
	checkError(err, "Creating kernel from mandelbrot.cl");

	size_t preferred_work_group_size_multiple;
	err = clGetKernelWorkGroupInfo(kernel, device, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(size_t), &preferred_work_group_size_multiple, NULL);
	checkError(err, "Getting preferred work group size multiple");
	printf("Preferred work group size multiple : %zu\n", preferred_work_group_size_multiple);

	//--------------------------------------------------------------------------------
	// Set arguments of the kernel
	//--------------------------------------------------------------------------------

	err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &b);
	err |= clSetKernelArg(kernel, 1, sizeof(int), &iteration);
	checkError(err, "Setting kernel args");

	//--------------------------------------------------------------------------------
	// Set work-groups number and size
	//--------------------------------------------------------------------------------

	const size_t global[2] = { width, height};
	const size_t local[2] = { local_width, local_height };
	printf("Work group dimensions : %zu %zu\n", global[0] / local[0], global[1] / local[1]);
	printf("Work group size : %zu %zu\n", local[0], local[1]);

	auto endInit = std::chrono::system_clock::now();
	std::chrono::duration<double> init_elapsed_seconds = endInit - startInit;

	//--------------------------------------------------------------------------------
	// Execution of the kernel
	//--------------------------------------------------------------------------------

	auto startComputation = std::chrono::system_clock::now();

	err = clEnqueueNDRangeKernel(
		commands,
		kernel,
		2, NULL,
		global, local,
		0, NULL, NULL);
	checkError(err, "Enqueueing kernel");

	// Wait compute kernel execution to end
	err = clFinish(commands);
	checkError(err, "Waiting for kernel to finish");

	auto endComputation = std::chrono::system_clock::now();
	std::chrono::duration<double> computation_elapsed_seconds = endComputation - startComputation;


	//--------------------------------------------------------------------------------
	// Get Data back
	//--------------------------------------------------------------------------------
	auto startGetData = std::chrono::system_clock::now();

	// Copy results from GPU buffer to Host buffer
	err = clEnqueueReadBuffer(
		commands, b, CL_TRUE, 0,
		bufferSize, buffer.data(),
		0, NULL, NULL);
	checkError(err, "Copying back buffer");

	auto endGetData = std::chrono::system_clock::now();
	std::chrono::duration<double> get_data_elapsed_seconds = endGetData - startGetData;

	//--------------------------------------------------------------------------------
	// End
	//--------------------------------------------------------------------------------

	auto endCL = std::chrono::system_clock::now();
	std::chrono::duration<double> CL_elapsed_seconds = endCL - startCL;

	//--------------------------------------------------------------------------------
	// Create Mandelbrot image with computed data
	//--------------------------------------------------------------------------------

	writeBufferAsBMP(buffer.data(), width, height, "mandelbrot_opencl.bmp");

	//--------------------------------------------------------------------------------
	// Results
	//--------------------------------------------------------------------------------

	printf("\n\n--------------- OpenCL RESULTS --------------------\n");
	printf("OpenCL Init duration: %f\n", init_elapsed_seconds.count());
	printf("OpenCL Computation duration: %f\n", computation_elapsed_seconds.count());
	printf("OpenCL Get Data duration: %f\n", get_data_elapsed_seconds.count());
	printf("Total OpenCL duration: %f\n", CL_elapsed_seconds.count());

	return 0;
}


int output_device_info(cl_device_id device_id)
{
	int err;                            // error code returned from OpenCL calls
	cl_device_type device_type;         // Parameter defining the type of the compute device
	cl_uint comp_units;                 // the max number of compute units on a device
	cl_char vendor_name[1024] = { 0 };    // string to hold vendor name for compute device
	cl_char device_name[1024] = { 0 };    // string to hold name of compute device
	cl_uint          max_work_itm_dims;
	size_t           max_wrkgrp_size;
	size_t* max_loc_size;

	// Get device name
	err = clGetDeviceInfo(device_id, CL_DEVICE_NAME, sizeof(device_name), &device_name, NULL);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to access device name!\n");
		return EXIT_FAILURE;
	}
	printf("Device Name : %s\n", device_name);

	// Get device type
	err = clGetDeviceInfo(device_id, CL_DEVICE_TYPE, sizeof(device_type), &device_type, NULL);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to access device type information!\n");
		return EXIT_FAILURE;
	}
	if (device_type == CL_DEVICE_TYPE_GPU)
		printf("Device type : GPU\n");

	else if (device_type == CL_DEVICE_TYPE_CPU)
		printf("Device type : CPU\n");

	else
		printf("Device type : Other\n");

	// Get device vendor name
	err = clGetDeviceInfo(device_id, CL_DEVICE_VENDOR, sizeof(vendor_name), &vendor_name, NULL);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to access device vendor name!\n");
		return EXIT_FAILURE;
	}
	printf("Vendor Name : %s\n", vendor_name);

	// Get device max compute units
	err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &comp_units, NULL);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to access device number of compute units !\n");
		return EXIT_FAILURE;
	}
	printf("Max compute units : %d\n", comp_units);

	err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(cl_uint),
		&max_work_itm_dims, NULL);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to get device Info (CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS)!\n, %s",
			err_code(err));
		return EXIT_FAILURE;
	}

	max_loc_size = (size_t*)malloc(max_work_itm_dims * sizeof(size_t));
	if (max_loc_size == NULL) {
		printf(" malloc failed\n");
		return EXIT_FAILURE;
	}
	err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_ITEM_SIZES, max_work_itm_dims * sizeof(size_t),
		max_loc_size, NULL);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to get device Info (CL_DEVICE_MAX_WORK_ITEM_SIZES)!\n, %s", err_code(err));
		return EXIT_FAILURE;
	}
	err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t),
		&max_wrkgrp_size, NULL);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to get device Info (CL_DEVICE_MAX_WORK_GROUP_SIZE)!\n, %s", err_code(err));
		return EXIT_FAILURE;
	}
	printf("Max work group size : ");
	for (unsigned int i = 0; i < max_work_itm_dims; i++)
		printf(" %d ", (int)(*(max_loc_size + i)));
	printf("\n");
	printf("Max invocations = %d\n", (int)max_wrkgrp_size);

	return CL_SUCCESS;

}
