__kernel void mandelbrot(__global float* buffer, int iterationMax)
{
    int i = get_global_id(0);
    int j = get_global_id(1);
    size_t width = get_global_size(0);
    size_t height = get_global_size(1);
    
    float cr = ((float)i / width) - 1.5;
    float ci = ((float)j / height) - 0.5;
    float x = 0.0f;
    float y = 0.0f;

    unsigned cmp = 0;
    while( x*x + y*y <= 4 && cmp <= iterationMax){
        float xtemp = x*x - y*y + cr;
        y = 2*x*y + ci;
        x = xtemp;
        ++cmp;
    }

    const float blue = (float)cmp/(float)iterationMax;

    buffer[(j * width + i) * 3] = 0.0f;
    buffer[(j * width + i) * 3 + 1] = 0.0f;
    buffer[(j * width + i) * 3 + 2] = blue;
}