__kernel void julia(__global float* buffer, int iterationMax)
{
    int i = get_global_id(0);
    int j = get_global_id(1);
    size_t width = get_global_size(0);
    size_t height = get_global_size(1);
    
    float cr = 0.292;
    float ci = 0.015;
    float x = (float)i / width;
    float y = (float)j / height;

    unsigned cmp = 0;
    while( x*x + y*y <= 4 && cmp <= iterationMax){
        float xtemp = x*x - y*y + cr;
        y = 2*x*y + ci;
        x = xtemp;
        ++cmp;
    }

    const float blue = (float)cmp/(float)iterationMax;

    buffer[(j * width + i) * 3] = 0.0f;
    buffer[(j * width + i) * 3 + 1] = 0.0f;
    buffer[(j * width + i) * 3 + 2] = blue;
}