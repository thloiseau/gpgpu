# GPGPU

The aim of this project is to see the speed up generated using GPU for the mandelbrot set generation

## Getting Started

Copy the project

```
git clone <url of the repository>
```

### Prerequisites

If you want to test the all project, first you need to install on your machine :
 - CUDA
 - VULKAN
 - OpenCL
 - SYCL

## Building the project

Build the project doing the following commands

```
mkdir build
cd build
cmake -G "Visual Studio 16 2019" ..
```

## Running the tests

Open GPGPU.sln in Visual Studio and that's it !