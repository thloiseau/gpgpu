#include <vulkan/vulkan.hpp>
#include <iostream>
#include <stdexcept>
#include <exception>
#include <fstream>
#include <chrono>
#include "bmp.hpp"

const char * const validationLayer = "VK_LAYER_KHRONOS_validation";
const uint32_t width = 1024;
const uint32_t height = 1024;
const uint32_t local_width = 32;
const uint32_t local_height = 32;
const uint32_t size = width * height * 3;
const uint32_t buffersize = size * sizeof(float); 
const std::string shaderName = "..\\VULKAN\\device\\mandelbrotWG.spv";

// Create buffer
VkBuffer createBuffer(const VkDevice& logicalDevice, uint32_t bufferSize, const VkBufferCreateInfo& bufferCreateInfo) {
	VkBuffer buffer;

	if (vkCreateBuffer(logicalDevice, &bufferCreateInfo, nullptr, &buffer) != VK_SUCCESS) {
		throw std::runtime_error("Buffer creation failed");
	}

	return buffer;
}

// Copy buffer
void copyBuffer(const VkBuffer& srcBuffer, VkBuffer& dstBuffer, const VkDevice& logicalDevice, int queueFamilyIndex) {

	// Command Pool

	VkCommandPool commandPool;

	VkCommandPoolCreateInfo commandPoolCreateInfo = {};
	commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	commandPoolCreateInfo.queueFamilyIndex = queueFamilyIndex;
	commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;

	if (vkCreateCommandPool(logicalDevice, &commandPoolCreateInfo, nullptr, &commandPool) != VK_SUCCESS) {
		throw std::runtime_error("Error during the Vulkan Instance creation.");
	}

	// Command buffer

	VkCommandBufferAllocateInfo commandBufferAllocationInfo = {};
	commandBufferAllocationInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	commandBufferAllocationInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	commandBufferAllocationInfo.commandPool = commandPool;
	commandBufferAllocationInfo.commandBufferCount = 1;

	VkCommandBuffer commandBuffer;
	vkAllocateCommandBuffers(logicalDevice, &commandBufferAllocationInfo, &commandBuffer);

	VkCommandBufferBeginInfo commandBufferBeginInfo = {};
	commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	if (vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo) != VK_SUCCESS) {
		throw std::runtime_error("Error during the Vulkan Instance creation.");
	}

	// Transfer command

	VkBufferCopy bufferCopy = {};
	bufferCopy.srcOffset = 0;
	bufferCopy.dstOffset = 0;
	bufferCopy.size = buffersize;
	vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &bufferCopy);

	if (vkEndCommandBuffer(commandBuffer) != VK_SUCCESS) {
		throw std::runtime_error("Error during the Vulkan Instance creation.");
	}

	// Submit the command buffer

	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;

	VkQueue queue;
	vkGetDeviceQueue(logicalDevice, queueFamilyIndex, 0, &queue);

	vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);

	vkQueueWaitIdle(queue);

	vkFreeCommandBuffers(logicalDevice, commandPool, 1, &commandBuffer);

	vkDestroyCommandPool(logicalDevice, commandPool, nullptr);
}

// Send data
void sendData(const VkDevice& logicalDevice, const VkPhysicalDevice& device, const VkDeviceMemory& bufferMemory, VkBuffer& buffer, int queueFamilyIndex, void* data) {

	// Staging buffer
	uint32_t stageBufferSize = buffersize;
	VkBufferCreateInfo stageBufferCreateInfo = {};
	stageBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	stageBufferCreateInfo.size = stageBufferSize;
	stageBufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
	stageBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	VkBuffer stageBuffer = createBuffer(logicalDevice, stageBufferSize, stageBufferCreateInfo);

	VkMemoryRequirements stageMemoryRequirements;
	vkGetBufferMemoryRequirements(logicalDevice, stageBuffer, &stageMemoryRequirements);

	VkPhysicalDeviceMemoryProperties stageMemoryProperties;
	vkGetPhysicalDeviceMemoryProperties(device, &stageMemoryProperties);

	VkMemoryType stageMemoryType;
	int stageMemoryTypeIndex = 0;

	for (VkMemoryType mem : stageMemoryProperties.memoryTypes) {
		if ((VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT & mem.propertyFlags) == VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) {
			if (stageMemoryRequirements.memoryTypeBits & (1u << stageMemoryTypeIndex)) {
				stageMemoryType = mem;
				break;
			}
		}
		++stageMemoryTypeIndex;
	}

	VkMemoryAllocateInfo stageMemoryAllocateInfo = {};
	stageMemoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	stageMemoryAllocateInfo.allocationSize = stageMemoryRequirements.size;
	stageMemoryAllocateInfo.memoryTypeIndex = stageMemoryTypeIndex;

	VkDeviceMemory stageBufferMemory;

	if (vkAllocateMemory(logicalDevice, &stageMemoryAllocateInfo, nullptr, &stageBufferMemory) != VK_SUCCESS) {
		std::runtime_error("Impossible to allocate memory");
	}

	vkBindBufferMemory(logicalDevice, stageBuffer, stageBufferMemory, 0);

	// Memory mapping
	void * driverData = new float[size];
	float * dataOut = reinterpret_cast<float *>(data);

	vkMapMemory(logicalDevice, stageBufferMemory, 0, stageBufferSize, 0, &driverData);
	for (int i = 0; i < size; ++i) {
		((float *)driverData)[i] = dataOut[i];
	}
	vkUnmapMemory(logicalDevice, stageBufferMemory);

	copyBuffer(stageBuffer, buffer, logicalDevice, queueFamilyIndex);
	
	vkFreeMemory(logicalDevice, stageBufferMemory, nullptr);
	vkDestroyBuffer(logicalDevice, stageBuffer, nullptr);
}

// Get data
void getData(const VkDevice& logicalDevice, const VkPhysicalDevice& device, const VkDeviceMemory& bufferMemory, VkBuffer& buffer, int queueFamilyIndex, void * data) {

	// Staging buffer
	uint32_t stageBufferSize = buffersize;
	VkBufferCreateInfo stageBufferCreateInfo = {};
	stageBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	stageBufferCreateInfo.size = stageBufferSize;
	stageBufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT;
	stageBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	VkBuffer stageBuffer = createBuffer(logicalDevice, stageBufferSize, stageBufferCreateInfo);

	VkMemoryRequirements stageMemoryRequirements;
	vkGetBufferMemoryRequirements(logicalDevice, stageBuffer, &stageMemoryRequirements);

	VkPhysicalDeviceMemoryProperties stageMemoryProperties;
	vkGetPhysicalDeviceMemoryProperties(device, &stageMemoryProperties);

	VkMemoryType stageMemoryType;
	int stageMemoryTypeIndex = 0;

	for (VkMemoryType mem : stageMemoryProperties.memoryTypes) {
		if ((VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT & mem.propertyFlags) == VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) {
			if (stageMemoryRequirements.memoryTypeBits & (1u << stageMemoryTypeIndex)) {
				stageMemoryType = mem;
				break;
			}
		}
		++stageMemoryTypeIndex;
	}

	VkMemoryAllocateInfo stageMemoryAllocateInfo = {};
	stageMemoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	stageMemoryAllocateInfo.allocationSize = stageMemoryRequirements.size;
	stageMemoryAllocateInfo.memoryTypeIndex = stageMemoryTypeIndex;

	VkDeviceMemory stageBufferMemory;

	if (vkAllocateMemory(logicalDevice, &stageMemoryAllocateInfo, nullptr, &stageBufferMemory) != VK_SUCCESS) {
		std::runtime_error("Impossible to allocate memory");
	}

	vkBindBufferMemory(logicalDevice, stageBuffer, stageBufferMemory, 0);

	// Memory mapping

	void * driverData = new float[size];
	float * dataIn = reinterpret_cast<float *>(data);

	copyBuffer(buffer, stageBuffer, logicalDevice, queueFamilyIndex);

	vkMapMemory(logicalDevice, stageBufferMemory, 0, stageBufferSize, 0, &driverData);
	for (int i = 0; i < size; ++i) {
		dataIn[i] = ((float *)driverData)[i];
	}
	vkUnmapMemory(logicalDevice, stageBufferMemory);

	vkFreeMemory(logicalDevice, stageBufferMemory, nullptr);
	vkDestroyBuffer(logicalDevice, stageBuffer, nullptr);
}

// Main function
int main() {

	VkResult result;

	auto startInit = std::chrono::system_clock::now();

	// Instance

	VkApplicationInfo applicationInfo = {};
	applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	applicationInfo.pApplicationName = "Vulkan lesson";
	applicationInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
	applicationInfo.pEngineName = "No Engine";
	applicationInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
	applicationInfo.apiVersion = VK_API_VERSION_1_0;

	VkInstanceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pApplicationInfo = &applicationInfo;
	createInfo.enabledExtensionCount = 0;
	createInfo.enabledLayerCount = 1;
	createInfo.ppEnabledLayerNames = &validationLayer;

	VkInstance instance;

	if (vkCreateInstance(&createInfo, nullptr, &instance) != VK_SUCCESS) {
		throw std::runtime_error("Error during the Vulkan Instance creation.");
	}

	// Physical devices

	uint32_t physicalDevicesCount = 0;
	vkEnumeratePhysicalDevices(instance, &physicalDevicesCount, nullptr);

	std::vector<VkPhysicalDevice> physicalDevices(physicalDevicesCount);
	vkEnumeratePhysicalDevices(instance, &physicalDevicesCount, physicalDevices.data());

	VkPhysicalDevice device = 0;
	VkPhysicalDeviceProperties physicalDeviceProperties;
	VkPhysicalDeviceFeatures physicalDeviceFeatures;

	for (VkPhysicalDevice physicalDevice : physicalDevices) {

		vkGetPhysicalDeviceProperties(physicalDevice, &physicalDeviceProperties);
		vkGetPhysicalDeviceFeatures(physicalDevice, &physicalDeviceFeatures);

		if (physicalDeviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
			device = physicalDevice;
		}
	}

	if (device == 0) {
		throw std::runtime_error("No GPU connected to the machine");
	}
	else {
		vkGetPhysicalDeviceProperties(device, &physicalDeviceProperties);
		vkGetPhysicalDeviceFeatures(device, &physicalDeviceFeatures);
		std::cout << "Connected to GPU" << physicalDeviceProperties.deviceName << "\n";
	}

	uint32_t* maxWorkGroupSize = physicalDeviceProperties.limits.maxComputeWorkGroupSize;
	uint32_t maxWorkGroupInvocations = physicalDeviceProperties.limits.maxComputeWorkGroupInvocations;

	std::cout << "Max Invocations :" << maxWorkGroupInvocations << std::endl;
	std::cout << "Work Group : x=" << maxWorkGroupSize[0] << " y=" << maxWorkGroupSize[1] << " z=" << maxWorkGroupSize[2] << std::endl;

	// Queue families

	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);

	std::vector<VkQueueFamilyProperties> queueFamilyProperties(queueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilyProperties.data());

	VkQueueFamilyProperties queueFamily;
	int queueFamilyIndex = 0;
	int i = 0;
	float priority = 1.0f;

	for (VkQueueFamilyProperties queueFam : queueFamilyProperties) {
		if (queueFam.queueFlags & VK_QUEUE_COMPUTE_BIT) {
			queueFamily = queueFam;
			queueFamilyIndex = i;
		}
		i++;
	}

	// Logical device and Queues

	VkDeviceQueueCreateInfo deviceQueueCreateInfo = {};
	VkDeviceCreateInfo deviceCreateInfo = {};
	VkDevice logicalDevice;
	VkQueue queue;

	deviceQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	deviceQueueCreateInfo.queueFamilyIndex = queueFamilyIndex;
	deviceQueueCreateInfo.queueCount = 1;
	deviceQueueCreateInfo.pQueuePriorities = &priority;

	deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	deviceCreateInfo.pQueueCreateInfos = &deviceQueueCreateInfo;
	deviceCreateInfo.queueCreateInfoCount = 1;
	deviceCreateInfo.pEnabledFeatures = &physicalDeviceFeatures;
	deviceCreateInfo.enabledExtensionCount = 0;
	deviceCreateInfo.enabledLayerCount = 0;

	if (vkCreateDevice(device, &deviceCreateInfo, nullptr, &logicalDevice) != VK_SUCCESS) {
		throw std::runtime_error("Impossible to create the device");
	}

	vkGetDeviceQueue(logicalDevice, queueFamilyIndex, 0, &queue);

	// Buffers

	uint32_t bufferSize = buffersize;
	VkBufferCreateInfo bufferCreateInfo = {};
	bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferCreateInfo.size = bufferSize;
	bufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
	bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	VkBuffer buffer = createBuffer(logicalDevice, bufferSize, bufferCreateInfo);

	// Buffer memory

	VkMemoryRequirements memoryRequirements;
	vkGetBufferMemoryRequirements(logicalDevice, buffer, &memoryRequirements);

	VkPhysicalDeviceMemoryProperties memoryProperties;
	vkGetPhysicalDeviceMemoryProperties(device, &memoryProperties);

	VkMemoryType memoryType;
	int memoryTypeIndex = 0;

	for (VkMemoryType mem : memoryProperties.memoryTypes) {
		if ((VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT & mem.propertyFlags) == VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) {
			if (memoryRequirements.memoryTypeBits & (1u << memoryTypeIndex)) {
				memoryType = mem;
				break;
			}
		}
		++memoryTypeIndex;
	}

	// Buffer memory allocation

	VkMemoryAllocateInfo memoryAllocateInfo = {};
	memoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	memoryAllocateInfo.allocationSize = memoryRequirements.size;
	memoryAllocateInfo.memoryTypeIndex = memoryTypeIndex;

	VkDeviceMemory bufferMemory;

	if (vkAllocateMemory(logicalDevice, &memoryAllocateInfo, nullptr, &bufferMemory) != VK_SUCCESS) {
		throw std::runtime_error("Impossible to allocate memory");
	}

	vkBindBufferMemory(logicalDevice, buffer, bufferMemory, 0);

	/************************************** TP4 **************************************/
	
	std::vector<float> data;
	std::vector<float> out(size);
	for (int i = 0; i < size; ++i) {
		data.push_back(i);
	}

	sendData(logicalDevice, device, bufferMemory, buffer, queueFamilyIndex, data.data());

	getData(logicalDevice, device, bufferMemory, buffer, queueFamilyIndex, out.data());


	/************************************** TP5 **************************************/

	// Load the shader

	std::ifstream file = std::ifstream(shaderName, std::ios::ate | std::ios::binary);
	size_t fileSize = (size_t)file.tellg();
	std::vector<char> code(fileSize);
	file.seekg(0);
	file.read(code.data(), fileSize);
	file.close();

	// Shader module

	const uint32_t * pCode = reinterpret_cast<const uint32_t*>(code.data());

	VkShaderModuleCreateInfo shaderModuleCreateInfo = {};
	shaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	shaderModuleCreateInfo.codeSize = ((code.size() + 3) /4) * 4;
	shaderModuleCreateInfo.pCode = pCode;

	VkShaderModule shaderModule;

	if(vkCreateShaderModule(logicalDevice, &shaderModuleCreateInfo, nullptr, &shaderModule) != VK_SUCCESS)
		throw std::runtime_error("Impossible to create shader module");


	/************************************** TP6 **************************************/
	
	// Descriptor set layout

	VkDescriptorSetLayoutBinding descriptorSetLayoutBinding = {};
	descriptorSetLayoutBinding.binding = 0;
	descriptorSetLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	descriptorSetLayoutBinding.descriptorCount = 1;
	descriptorSetLayoutBinding.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;
	descriptorSetLayoutBinding.pImmutableSamplers = nullptr;

	VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo = {};
	descriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	descriptorSetLayoutCreateInfo.bindingCount = 1;
	descriptorSetLayoutCreateInfo.pBindings = &descriptorSetLayoutBinding;

	VkDescriptorSetLayout descriptorSetLayout = {};

	if (vkCreateDescriptorSetLayout(logicalDevice, &descriptorSetLayoutCreateInfo, nullptr, &descriptorSetLayout) != VK_SUCCESS) {
		throw std::runtime_error("Impossible to set the descriptor layout");
	}


	// Descriptor pool

	VkDescriptorPoolSize descriptorPoolSize = {};
	descriptorPoolSize.descriptorCount = 1;
	descriptorPoolSize.type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;

	VkDescriptorPoolCreateInfo descriptorPoolCreateInfo = {};
	descriptorPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	descriptorPoolCreateInfo.poolSizeCount = 1;
	descriptorPoolCreateInfo.pPoolSizes = &descriptorPoolSize;
	descriptorPoolCreateInfo.maxSets = 1;

	VkDescriptorPool descriptorPool = {};

	if (vkCreateDescriptorPool(logicalDevice, &descriptorPoolCreateInfo, nullptr, &descriptorPool) != VK_SUCCESS) {
		throw std::runtime_error("Impossible to set the descriptor pool");
	}

	// Allocate the descriptor set

	VkDescriptorSetAllocateInfo descriptorSetAllocateInfo = {};
	descriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	descriptorSetAllocateInfo.descriptorPool = descriptorPool;
	descriptorSetAllocateInfo.descriptorSetCount = 1;
	descriptorSetAllocateInfo.pSetLayouts = &descriptorSetLayout;

	VkDescriptorSet descriptorSet = {};

	if (vkAllocateDescriptorSets(logicalDevice, &descriptorSetAllocateInfo, &descriptorSet) != VK_SUCCESS) {
		throw std::runtime_error("Impossible to allocate the descriptor set");
	}


	// Fill the descriptor set

	VkDescriptorBufferInfo descriptorBufferInfo = {};
	descriptorBufferInfo.buffer = buffer;
	descriptorBufferInfo.offset = 0;
	descriptorBufferInfo.range = bufferSize;

	VkWriteDescriptorSet writeDescriptorSet = {};
	writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeDescriptorSet.dstSet = descriptorSet;
	writeDescriptorSet.dstBinding = 0;
	writeDescriptorSet.dstArrayElement = 0;
	writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	writeDescriptorSet.descriptorCount = 1;
	writeDescriptorSet.pBufferInfo = &descriptorBufferInfo;

	vkUpdateDescriptorSets(logicalDevice, 1, &writeDescriptorSet, 0, nullptr);

	// Compute pipeline

	VkPushConstantRange pushConstantRange = {};
	pushConstantRange.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;
	pushConstantRange.offset = 0;
	pushConstantRange.size = 2 * sizeof(unsigned int);

	VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = {};
	pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutCreateInfo.setLayoutCount = 1;
	pipelineLayoutCreateInfo.pSetLayouts = &descriptorSetLayout;
	pipelineLayoutCreateInfo.pushConstantRangeCount = 1;
	pipelineLayoutCreateInfo.pPushConstantRanges = &pushConstantRange;

	VkPipelineLayout pipelineLayout;

	if (vkCreatePipelineLayout(logicalDevice, &pipelineLayoutCreateInfo, nullptr, &pipelineLayout) != VK_SUCCESS) {
		throw std::runtime_error("Impossible to create the pipeline layout");
	}

	VkPipelineShaderStageCreateInfo pipelineShaderStageCreateInfo = {};
	pipelineShaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	pipelineShaderStageCreateInfo.stage = VK_SHADER_STAGE_COMPUTE_BIT;
	pipelineShaderStageCreateInfo.module = shaderModule;
	pipelineShaderStageCreateInfo.pName = "main";

	VkComputePipelineCreateInfo computePipelineCreateInfo = {};
	computePipelineCreateInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
	computePipelineCreateInfo.stage = pipelineShaderStageCreateInfo;
	computePipelineCreateInfo.layout = pipelineLayout;

	VkPipeline pipeline;

	if (vkCreateComputePipelines(logicalDevice, VK_NULL_HANDLE, 1, &computePipelineCreateInfo, nullptr, &pipeline) != VK_SUCCESS) {
		throw std::runtime_error("Impossible to create the pipeline");
	}


	// Command Buffer

	VkCommandPool commandPool;

	VkCommandPoolCreateInfo commandPoolCreateInfo = {};
	commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	commandPoolCreateInfo.queueFamilyIndex = queueFamilyIndex;
	commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;

	if (vkCreateCommandPool(logicalDevice, &commandPoolCreateInfo, nullptr, &commandPool) != VK_SUCCESS) {
		throw std::runtime_error("Error during the Vulkan Instance creation.");
	}

	VkCommandBufferAllocateInfo commandBufferAllocationInfo = {};
	commandBufferAllocationInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	commandBufferAllocationInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	commandBufferAllocationInfo.commandPool = commandPool;
	commandBufferAllocationInfo.commandBufferCount = 1;

	VkCommandBuffer commandBuffer;
	vkAllocateCommandBuffers(logicalDevice, &commandBufferAllocationInfo, &commandBuffer);

	VkCommandBufferBeginInfo commandBufferBeginInfo = {};
	commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	if (vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo) != VK_SUCCESS) {
		throw std::runtime_error("Error during the Vulkan Instance creation.");
	}

	vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline);
	vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipelineLayout, 0, 1, &descriptorSet, 0, nullptr);
	uint32_t tmp[] = { width, height };
	vkCmdPushConstants(commandBuffer, pipelineLayout, VK_SHADER_STAGE_COMPUTE_BIT, 0, 2 * sizeof(uint32_t), tmp);
	vkCmdDispatch(commandBuffer, width / local_width, height / local_height, 1);
	vkEndCommandBuffer(commandBuffer);


	// Run the command buffer
	// Fence

	VkFence fence;
	VkFenceCreateInfo fenceCreateInfo = {};
	fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	vkCreateFence(logicalDevice, &fenceCreateInfo, nullptr, &fence);
	
	// Submit and wait fence

	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;
	submitInfo.pWaitSemaphores = nullptr;
	submitInfo.waitSemaphoreCount = 0;
	submitInfo.pWaitDstStageMask = nullptr;
	submitInfo.signalSemaphoreCount = 0;
	submitInfo.pSignalSemaphores = nullptr;

	auto endInit = std::chrono::system_clock::now();
	std::chrono::duration<double> init_elapsed_seconds = endInit - startInit;

	auto startComputation = std::chrono::system_clock::now();

	vkQueueSubmit(queue, 1, &submitInfo, fence);
	vkWaitForFences(logicalDevice, 1, &fence, true, uint64_t(-1));

	auto endComputation = std::chrono::system_clock::now();
	std::chrono::duration<double> compuation_elapsed_seconds = endComputation - startComputation;
	
	// Result

	std::vector<float> results(size);

	auto startGetData = std::chrono::system_clock::now();

	getData(logicalDevice, device, bufferMemory, buffer, queueFamilyIndex, results.data());

	auto endGetData = std::chrono::system_clock::now();
	std::chrono::duration<double> get_data_elapsed_seconds = endGetData - startGetData;

	writeBufferAsBMP(results.data(), width, height, "test.bmp");

	printf("\n\n--------------- RESULTS --------------------\n");
	printf("Initialization duration: %f\n", init_elapsed_seconds.count());
	printf("Computation duration: %f\n", compuation_elapsed_seconds.count());
	printf("Get Data duration: %f\n", get_data_elapsed_seconds.count());

	// Clean

	vkFreeCommandBuffers(logicalDevice, commandPool, 1, &commandBuffer);
	vkDestroyCommandPool(logicalDevice, commandPool, nullptr);
	vkDestroyFence(logicalDevice, fence, nullptr);
	vkDestroyPipeline(logicalDevice, pipeline, nullptr);
	vkDestroyPipelineLayout(logicalDevice, pipelineLayout, nullptr);
	vkDestroyDescriptorPool(logicalDevice, descriptorPool, nullptr);
	vkDestroyDescriptorSetLayout(logicalDevice, descriptorSetLayout, nullptr);
	vkDestroyShaderModule(logicalDevice, shaderModule, nullptr);
	vkFreeMemory(logicalDevice, bufferMemory, nullptr);
	vkDestroyBuffer(logicalDevice, buffer, nullptr);
	vkDestroyDevice(logicalDevice, nullptr);
	vkDestroyInstance(instance, nullptr);

	return 0;
	
}
