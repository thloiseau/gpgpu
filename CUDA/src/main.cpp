#include <iostream>
#include <vector>
#include "bmp.hpp"

const size_t width = 1024;
const size_t height = 1024;
const size_t local_width = 1;
const size_t local_height = 1;
const size_t size = width * height;
const size_t imageSize = size * 3;
const size_t bufferSize = size * sizeof(float);

__global__ void mandelbrot(float* buffer, int iterationMax) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    size_t width = gridDim.x * blockDim.x;
    size_t height = gridDim.y * blockDim.y;

    float cr = ((float)i / width) - 1.5;
    float ci = ((float)j / height) - 0.5;
    float x = 0.0f;
    float y = 0.0f;

    unsigned cmp = 0;
    while (x * x + y * y <= 4 && cmp <= iterationMax) {
        float xtemp = x * x - y * y + cr;
        y = 2 * x * y + ci;
        x = xtemp;
        ++cmp;
    }

    const float green = (float)cmp / (float)iterationMax;

    buffer[(j * width + i) * 3] = 0.0f;
    buffer[(j * width + i) * 3 + 1] = green;
    buffer[(j * width + i) * 3 + 2] = 0.0f;
}

int main(void) {
	std::vector<float> image(imageSize);

	float* buffer;

	cudaMalloc(&buffer, bufferSize);
    
    dim3 grid(width / local_width, height / local_height);
    dim3 blocks(local_width, local_height);

	mandelbrot<<<grid, blocks>>>(buffer, 40);

	cudaMemcpy(image.data(), buffer, bufferSize, cudaMemcpyDeviceToHost);

    writeBufferAsBMP(image.data(), width, height, "mandelbrot_cuda.bmp");

	cudaFree(buffer);
}